﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private const int _initialBalls = 3;

    private int _score;
    private int _level;
    private int _balls;

    private bool _isSwitchingState;
    private State _state;
    private GameObject _currentLevel;
    private GameObject _currentBall;

    public GameObject BallPerefab;
    public GameObject PaddlePrefab;

    public GameObject[] Levels;

    public GameObject PanelMenu;
    public GameObject PanelPlay;
    public GameObject PanelLevelComplete;
    public GameObject PanelGameOver;
    public GameObject PanelExit;

    public Text ScoreText;
    public Text BallsText;
    public Text LevelText;
    public Text HighScoreText;

    public static GameManager Instance { get; private set; }

    public int Score
    {
        get => _score;
        set
        {
            _score = value;
            this.ScoreText.text = "SCORE: " + _score;
        }
    }
    public int Level
    {
        get => _level;
        set
        {
            _level = value;
            this.LevelText.text = "LEVEL " + _level;
        }
    }
    public int Balls
    {
        get => _balls;
        set
        {
            _balls = value;
            this.BallsText.text = "Balls: " + _balls;
        }
    }

    public enum State { MENU, INIT, PLAY, LEVELCOMPLETE, LOADLEVEL, GAMEOVER, EXIT }

    public void PlayClicked()
    {
        this.SwitchState(State.INIT);
    }

    public void SwitchState(State newState)
    {
        _isSwitchingState = true;

        this.EndState();
        _state = newState;
        this.BeginState(newState);

        _isSwitchingState = false;
    }


    private bool BallIsLost()
    {
        return _currentBall == null && !_isSwitchingState;
    }

    private void BeginState(State newState)
    {
        switch (newState)
        {
            case State.MENU:
                this.LoadMenu();
                break;

            case State.INIT:
                this.Initialize();
                this.SwitchState(State.LOADLEVEL);
                break;

            case State.PLAY:
                this.PanelPlay.SetActive(true);
                break;

            case State.LEVELCOMPLETE:
                this.LoadLevelCompletePanel();
                Invoke("LoadNextLevel", 3f);
                break;

            case State.LOADLEVEL:
                this.LoadLevel();
                break;

            case State.GAMEOVER:
                this.UpdateHighScore();
                this.PanelGameOver.SetActive(true);
                break;

            case State.EXIT:
                this.PanelExit.SetActive(true);
                break;

            default:
                break;
        }

    }

    private void CheckBallState()
    {
        if (this.BallIsLost())
        {
            if (this.Balls > 0)
                this.GetNewBall();
            else
                this.SwitchState(State.GAMEOVER);
        }
    }

    private void CheckIfLevelComplete()
    {
        if (_currentLevel != null && !_isSwitchingState && _currentLevel.transform.GetChild(0).transform.childCount == 0)
            this.SwitchState(State.LEVELCOMPLETE);
    }

    private void DestroyGameObjects()
    {
        Destroy(_currentLevel);
        Destroy(_currentBall);
    }

    private void EndState()
    {
        switch (_state)
        {
            case State.MENU:
                this.PanelMenu.SetActive(false);
                break;

            case State.LEVELCOMPLETE:
                this.PanelLevelComplete.SetActive(false);
                break;

            case State.GAMEOVER:
                this.PanelGameOver.SetActive(false);
                this.PanelPlay.SetActive(false);
                break;

            default:
                break;
        }
    }

    private void GetNewBall()
    {
        _currentBall = Instantiate(this.BallPerefab);
        this.Balls--;
    }

    private void Initialize()
    {
        this.PanelPlay.SetActive(true);

        this.Score = 0;
        this.Level = 0;
        this.Balls = _initialBalls;

        Instantiate(this.PaddlePrefab);
    }

    private void InstantiateCurrentLevel()
    {
        this.Level++;
        _currentLevel = Instantiate(this.Levels[this.Level - 1]);
    }

    private void LoadLevel()
    {
        if (this.NoMoreLevels())
            this.SwitchState(State.GAMEOVER);
        else
        {
            this.InstantiateCurrentLevel();
            this.SwitchState(State.PLAY);
        }
    }

    private void LoadLevelCompletePanel()
    {
        this.DestroyGameObjects();
        this.PanelLevelComplete.SetActive(true);
    }

    private void LoadMenu()
    {
        this.DestroyGameObjects();
        this.UpdateHighScoreText();
        Cursor.visible = true;
        this.PanelMenu.SetActive(true);
    }

    private void LoadNextLevel()
    {
        this.Balls++;
        this.SwitchState(State.LOADLEVEL);
    }

    private bool NoMoreLevels()
    {
        return this.Level >= this.Levels.Length;
    }

    private void ReadKeyboardForExit()
    {
        if (Input.GetKeyUp(KeyCode.Y)) // || Input.GetKeyUp(KeyCode.Escape)) -- This sometimes causes the game to instantly exit by mistake
        {
            Application.Quit();
        }

        else if (Input.GetKeyUp(KeyCode.N))
        {
            this.PanelExit.SetActive(false);
            SwitchState(State.PLAY);
        }
    }

    private void Start()
    {
        Instance = this;
        this.SwitchState(State.MENU);
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
            SwitchState(State.EXIT);

        switch (_state)
        {
            case State.PLAY:
                this.CheckBallState();
                this.CheckIfLevelComplete();
                break;

            case State.GAMEOVER:
                this.WaitForAnyKey();
                break;

            case State.EXIT:
                this.ReadKeyboardForExit();
                break;

            default:
                break;
        }
    }

    private void UpdateHighScore()
    {
        if (this.Score > PlayerPrefs.GetInt("highScore"))
            PlayerPrefs.SetInt("highScore", this.Score);
    }

    private void UpdateHighScoreText()
    {
        this.HighScoreText.text = $"HIGHSCORE <{PlayerPrefs.GetInt("highScore")}>";
    }

    private void WaitForAnyKey()
    {
        if (Input.anyKeyDown)
            SwitchState(State.MENU);
    }

}
