﻿using UnityEngine;

public class Brick : MonoBehaviour
{
    private Material _originalMaterial;
    private Renderer _renderer;

    public int HitsToDestroy;
    public int Points;
    public Vector3 Rotator;
    public Material HitMaterial;

    private void ConsumeBrick()
    {
        GameManager.Instance.Score += this.Points;
        Destroy(this.gameObject);
    }

    private void HitBrick()
    {
        this.HitsToDestroy--;

        if (this.HitsToDestroy <= 0)
            this.ConsumeBrick();
    }

    private void OnCollisionEnter(Collision other)
    {
        this.SwichToHitMaterial();
        this.HitBrick();
        Invoke("RestoreMaterial", 0.05f);
    }

    private void RestoreMaterial()
    {
        _renderer.sharedMaterial = _originalMaterial;
    }

    private void Rotate()
    {
        transform.Rotate(this.Rotator * Time.deltaTime * (transform.position.y + transform.position.x) * 0.1f);
    }

    private void Start()
    {
        _renderer = GetComponent<Renderer>();
        _originalMaterial = _renderer.sharedMaterial;
    }

    private void SwichToHitMaterial()
    {
        _renderer.sharedMaterial = HitMaterial;
    }

    private void Update()
    {
        this.Rotate();
    }
}
