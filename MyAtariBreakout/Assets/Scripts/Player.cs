﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private const float _yPosOfPaddle = -8.3f;
    private Rigidbody _rigidbody;

    private bool _movingWithMouse = true;
    private float _xPosOfCursor;

    private void CheckIfMovingWithMouse()
    {
        float currentPosOfCursor = this.GetXPositionOfCursorInField();

        if (_xPosOfCursor != currentPosOfCursor)
        {
            _xPosOfCursor = currentPosOfCursor;
            _movingWithMouse = true;
        }

        else if (Input.anyKey)
        {
            _movingWithMouse = false;
        }
    }

    private void FixedUpdate()
    {
        this.RemovePaddleVelocity();

        this.CheckIfMovingWithMouse();

        if (_movingWithMouse)
            this.MoveWithMouse();
        else
            this.ReadKeyboard();
    }

    private float GetXPositionOfCursorInField()
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, _yPosOfPaddle, 0)).x;
    }

    private void MoveLeft()
    {
        _rigidbody.MovePosition(new Vector3(_rigidbody.position.x - 0.5f, _yPosOfPaddle, 0));
    }

    private void MoveRight()
    {
        _rigidbody.MovePosition(new Vector3(_rigidbody.position.x + 0.5f, _yPosOfPaddle, 0));
    }

    private void MoveWithMouse()
    {
        _rigidbody.MovePosition(new Vector3(this.GetXPositionOfCursorInField(), _yPosOfPaddle, 0));
    }

    private void ReadKeyboard()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            this.MoveRight();

        else if (Input.GetKey(KeyCode.LeftArrow))
            this.MoveLeft();
    }

    private void RemovePaddleVelocity()
    {
        _rigidbody.velocity = new Vector3(0, 0, 0);
    }

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _xPosOfCursor = this.GetXPositionOfCursorInField();
        Cursor.visible = false;
    }
}
