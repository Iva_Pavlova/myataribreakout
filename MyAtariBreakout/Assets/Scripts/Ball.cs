﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private const float _minSpeed = 8f;
    private const float _initialSpeed = 20f;
    private const float _launchDelay = 1.5f;

    private float _speed = 20f;
    private Rigidbody _rigidbody;
    private Vector3 _velocity;
    private Renderer _renderer;

    private bool _isLaunching = true;

    private bool BallIsVisible()
    {
        return _renderer.isVisible;
    }

    private void CheckSpeed()
    {
        if (this.SpeedIsTooLow())
            this.FixSpeedToInitialValue();
    }

    private void FixedUpdate()
    {
        this.UpdateVelocity();

        if (!this.BallIsVisible())
            Destroy(gameObject);
    }

    private void FixSpeedToInitialValue()
    {
        _rigidbody.velocity = Vector3.down * _initialSpeed;
    }

    private void Launch()
    {
        _rigidbody.velocity = Vector3.down * _initialSpeed;
        _isLaunching = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        this.ReflectVelocity(collision);
    }

    private void ReflectVelocity(Collision collision)
    {
        _rigidbody.velocity = Vector3.Reflect(this._velocity, collision.contacts[0].normal);
    }

    private bool SpeedIsTooLow()
    {
        return Mathf.Abs(_rigidbody.velocity.y) < _minSpeed;
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponent<Renderer>();

        Invoke("Launch", _launchDelay);
    }

    private void Update()
    {
        if (!_isLaunching)
            this.CheckSpeed();
    }

    private void UpdateVelocity()
    {
        _velocity = _rigidbody.velocity.normalized * _speed;
    }
}
